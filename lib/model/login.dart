class Login {
  final bool success;
  final String message;
  final String token;

  Login({
    this.success,
    this.message,
    this.token
  });

  factory Login.fromJson(Map<String, dynamic> json){
    return Login(
      success: json['success'],
      message: json['message'],
      token: json['token'],
    );
  }

  @override
  String toString() {
    return 'Login{success: $success, message: $message, token: $token}';
  }
}