import 'package:flutter/cupertino.dart';

class DetailPhoto {
  String id;
  String title;
  String price;
  String resolution;
  String uploader;
  String description;

  DetailPhoto(this.id, this.title, this.price, this.resolution, this.uploader, this.description);
}