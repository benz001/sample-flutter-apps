class OperationPhoto {
  final bool success;
  final String message;

  OperationPhoto({
    this.success,
    this.message
  });

  factory OperationPhoto.fromJson(Map<String, dynamic> json){
    return OperationPhoto(
      success: json['success'],
      message: json['message'],
    );
  }

  @override
  String toString() {
    return 'OperationPhoto{success: $success, message: $message}';
  }
}