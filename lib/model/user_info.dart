class UserInfo {
  final String name;
  final String email;
  final String phone;
  final String type;

  UserInfo({
    this.name,
    this.email,
    this.phone,
    this.type
  });

  factory UserInfo.fromJson(Map<String, dynamic> json){
    return UserInfo(
      name: json['name'],
      email: json['email'],
      phone: json['phone'],
      type: json['type']
    );
  }

  @override
  String toString() {
    return 'UserInfo{name: $name, email: $email, phone: $phone, type: $type}';
  }
}