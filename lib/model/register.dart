class Register {
  final bool success;
  final String message;
  final String token;

  Register({
    this.success,
    this.message,
    this.token
  });

  factory Register.fromJson(Map<String, dynamic> json){
    return Register(
      success: json['success'],
      message: json['message'],
      token: json['token'],
    );
  }

  @override
  String toString() {
    return 'Register{success: $success, message: $message, $token: $token}';
  }
}