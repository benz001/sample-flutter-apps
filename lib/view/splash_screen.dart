import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view/buyer/view_for_buyer_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = '/splashScreen';

  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var apiServices = APIServices();

   _saveEmail(paramEmail) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('email', paramEmail);
  }


  Future<String> _getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token = prefs.getString('token');
    return _token;
  }

  Future<void> _getUserInfo() async {
    _getToken().then((value) => _checkUserType(value));
  }

  Future<void> _checkUserType(String token) async {
    final response = await http.get(apiServices.baseUrl+'/user/info',
        headers: {'Authorization': 'bearer $token'});
        print(response.statusCode);
        if (response.statusCode == 200) {
          var result = jsonDecode(response.body);
          print(result['type']);
          String type = result['type']; 
          String email = result['email'];
          print(email);
          _saveEmail(email);
          if (type == 'seller') {
            Navigator.popAndPushNamed(context, MainMenuScreen.routeName);
          } else if(type == 'buyer'){
            Navigator.popAndPushNamed(context, ViewForBuyerScreen.routeName);
          }else{
            print('unidentification');
          }
        } else {
          print('failed');
        }
  }

  @override
  void initState() {
    super.initState();
    _getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      // appBar: AppBar(),
      body: Container(
        alignment: Alignment.center,
        width: width,
        height: height,
        child: Text(
          'Welcome to Catalogue App',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
