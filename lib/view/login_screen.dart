import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view/buyer/view_for_buyer_screen.dart';
import 'package:sample_flutter_app/view/register_screen.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter_app/view/splash_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/loginScreen';
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String dropdownValue = 'Buyer';
  List<String> spinnerItems = ['Buyer', 'Seller'];
  var apiServices = APIServices();

  var _tfEmail = TextEditingController();
  var _tfPassword = TextEditingController();


  Future<void> _loginProccess() async {
    apiServices.login(_tfEmail.text, _tfPassword.text).then((value) {
      if (value.success == true) {
        _saveToken(value.token);
        Navigator.popAndPushNamed(context, SplashScreen.routeName);
      } else {
        print('Login Failed');
      }
    });
  }

  _saveToken(paramToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', paramToken);
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Container(
          width: width,
          height: height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Wellcome, To Catalogue App',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                // height: 100,
                // color: Colors.red,
                child: TextField(
                  controller: _tfEmail,
                  decoration: InputDecoration(
                      hintText: 'email', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                // height: 100,
                // color: Colors.red,
                child: TextField(
                  obscureText: true,
                  controller: _tfPassword,
                  decoration: InputDecoration(
                      hintText: 'password', border: OutlineInputBorder()),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 15,
              ),
              Stack(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: width,
                    height: height * 0.08,
                    child: RaisedButton(
                      color: Colors.green,
                      onPressed: () {
                        _loginProccess();
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Dont't have account?"),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, RegisterScreen.routeName);
                      },
                      child: Text(
                        'Create Account',
                        style: TextStyle(color: Colors.blue),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
