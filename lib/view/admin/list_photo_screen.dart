import 'package:flutter/material.dart';
import 'package:sample_flutter_app/model/arguments.dart';
import 'package:sample_flutter_app/model/detail_photo.dart';
import 'package:sample_flutter_app/view/admin/edit_photo_screen.dart';
import 'package:sample_flutter_app/view/admin/main_menu_screen.dart';
import 'package:sample_flutter_app/view_model/api_services.dart';
import 'package:sample_flutter_app/widget/alert_dialog_show.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListPhotoScreen extends StatefulWidget {
  static const routeName = '/listPhotoScreen';
  // const ListPhotoScreen({Key key}) : super(key: key);

  @override
  _ListPhotoScreenState createState() => _ListPhotoScreenState();
}

class _ListPhotoScreenState extends State<ListPhotoScreen> {
  var alertDialogShow = AlertDialogShow();
  var apiServices = APIServices();
  String dropdownValue = 'Desc Post Date';
  List<String> spinnerItems = [
    'Asc Title',
    'Desc Title',
    'Asc Post Date',
    'Desc Post Date'
  ];
  List<dynamic> listPhoto;

  void _getListPhoto() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('token');
    await apiServices.listPhoto(_token).then((value) {
      setState(() {
        listPhoto = value.data;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getListPhoto();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Future<void> _confirmDeleteDialog(
        String title, String positive, String negative, String id) async {
      return showDialog(
        context: context,
        barrierDismissible: false, // user must tap button for close dialog!
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            // content: Text(content),
            actions: <Widget>[
              FlatButton(
                  child: Text(positive),
                  onPressed: () {
                    apiServices.deletePhoto(id).then((value) {
                      if (value.success) {
                        Navigator.popAndPushNamed(
                            context, MainMenuScreen.routeName);
                        // Navigator.of(context).pop();
                      } else {
                        print('Failed');
                      }
                    });
                  }),
              FlatButton(
                child: Text(negative),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }

    Widget _itemPhoto(List listPhoto, int index) {
      return Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Container(
          width: width,
          height: 150,
          child: Card(
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 4,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[100],
                          image: DecorationImage(
                              image: NetworkImage(apiServices.baseUrlAlt +
                                  '/photos' +
                                  '/' +
                                  listPhoto[index]['photo']))),
                    )),
                Expanded(
                    flex: 6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 7,
                          child: Container(
                            // width: 100,
                            // color: Colors.red,
                            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(listPhoto[index]['title'],
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                Text("Rp." +
                                    listPhoto[index]['price'].toString()),
                                Text(listPhoto[index]['resolution'] + '')
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                RaisedButton(
                                  color: Colors.green,
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, EditPhotoScreen.routeName,
                                        arguments: DetailPhoto(
                                            listPhoto[index]['id'].toString(),
                                            listPhoto[index]['title'],
                                            listPhoto[index]['price'],
                                            listPhoto[index]['resolution'],
                                            listPhoto[index]['uploader'],
                                            listPhoto[index]['description']));
                                    // print(listPhoto[index]['id']);
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                RaisedButton(
                                  color: Colors.red,
                                  onPressed: () {
                                    _confirmDeleteDialog(
                                        'Are you sure to delete this photo ?',
                                        'Yes',
                                        'No',
                                        listPhoto[index]['id'].toString());
                                  },
                                  child:
                                      Icon(Icons.delete, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
        ),
      );
    }

    Widget _searchPhoto() {
      return Container(
        width: width,
        height: height * 0.1,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(5),
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.search),
                hintText: 'Search Photo'),
          ),
        ),
      );
    }

    Widget _sortPhoto() {
      return Container(
        width: width,
        height: height * 0.1,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Expanded(flex: 2, child: Text('Sort By: ')),
              Expanded(
                flex: 8,
                child: Container(
                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.grey)),
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: dropdownValue,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    elevation: 16,
                    // style: TextStyle(color: Colors.red, fontSize: 18),
                    underline: Container(
                      height: 2,
                      color: Colors.transparent,
                    ),
                    onChanged: (String data) {
                      setState(() {
                        dropdownValue = data;
                      });
                    },
                    items: spinnerItems
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Column(
      children: <Widget>[
        _searchPhoto(),
        _sortPhoto(),
        Container(
          width: width,
          height: height * 0.55,
          color: Colors.grey,
          child: ListView.builder(
              itemCount: listPhoto == null ? 0 : listPhoto.length,
              itemBuilder: (BuildContext context, int index) {
                return _itemPhoto(listPhoto, index);
              }),
        ),

        // FutureBuilder(builder: null)),
      ],
    );
    //   floatingActionButton: FloatingActionButton(
    //     onPressed: () {
    //       Navigator.pushNamed(context, AddPhotoScreen.routeName);
    //     },
    //     child: Icon(Icons.add),
    // );
  }
}
