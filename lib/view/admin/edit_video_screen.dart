import 'package:flutter/material.dart';

class EditVideoScreen extends StatefulWidget {
  static const routeName = '/editVideoScreen';
  const EditVideoScreen({Key key}) : super(key: key);

  @override
  _EditVideoScreenState createState() => _EditVideoScreenState();
}

class _EditVideoScreenState extends State<EditVideoScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Edit Video'),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        // child: Container(
          // width: width,
          // height: height,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Add Your Information Video',
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: width,
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: 'Title', border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: width,
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: 'Price', border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: width,
                  child: RaisedButton(onPressed: (){}, child: Text('Choose Video'),)
                ),
                 SizedBox(
                  height: 15,
                ),
                 Container(
                  width: width,
                  child: RaisedButton(onPressed: (){}, child: Text('Choose Thumbnail Video'),)
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Container(
                        // width: width,
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: 'Width', border: OutlineInputBorder()),
                        ),
                      ),
                    ),
                    Expanded(
                        flex: 2,
                        child: Text(
                          'X',
                          textAlign: TextAlign.center,
                        )),
                    Expanded(
                      flex: 4,
                      child: Container(
                        // width: width,
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: 'Height', border: OutlineInputBorder()),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: width,
                  child: TextField(
                    maxLines: 5,
                    decoration: InputDecoration(
                        hintText: 'Description', border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Stack(
                  children: <Widget>[
                    ButtonTheme(
                      minWidth: width,
                      child: RaisedButton(
                        color: Colors.green,
                        onPressed: () {},
                        child: Text(
                          'Add',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        // ),
      ),
    );
  }
}
