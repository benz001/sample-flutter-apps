import 'package:flutter/material.dart';
import 'package:sample_flutter_app/widget/alert_dialog_show.dart';

class ListViewPhotoScreen extends StatefulWidget {
  static const routeName = '/listViewPhotoScreen';
  // const ListViewPhotoScreen({Key key}) : super(key: key);

  @override
  _ListViewPhotoScreenState createState() => _ListViewPhotoScreenState();
}

class _ListViewPhotoScreenState extends State<ListViewPhotoScreen> {
  var alertDialogShow = AlertDialogShow();
  String dropdownValue = 'Desc Post Date';
  List<String> spinnerItems = [
    'Asc Title',
    'Desc Title',
    'Asc Post Date',
    'Desc Post Date',
    'Asc Duration',
    'Desc Duration'
  ];
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Widget _itemPhoto() {
      return Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Container(
          width: width,
          height: 150,
          child: Card(
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 4,
                    child: Container(
                      color: Colors.grey[100],
                    )),
                Expanded(
                    flex: 6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          // flex: 7,
                          child: Container(
                            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Title',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                Text('Post Date'),
                                Text('Size')
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                            child: Container(
                          alignment: Alignment.center,
                          child: RaisedButton(
                            color: Colors.orange,
                            onPressed: () {

                            },
                            child: Icon(Icons.zoom_in, color: Colors.white,),
                          ),
                        ))
                      ],
                    ))
              ],
            ),
          ),
        ),
      );
    }

    Widget _searchPhoto() {
      return Container(
        width: width,
        height: height * 0.08,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(5),
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.search),
                hintText: 'Search Photo'),
          ),
        ),
      );
    }

    Widget _sortPhoto() {
      return Container(
        width: width,
        height: height * 0.08,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Expanded(flex: 2, child: Text('Sort By: ')),
              Expanded(
                flex: 8,
                child: Container(
                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.grey)),
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: dropdownValue,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    elevation: 16,
                    // style: TextStyle(color: Colors.red, fontSize: 18),
                    underline: Container(
                      height: 2,
                      color: Colors.transparent,
                    ),
                    onChanged: (String data) {
                      setState(() {
                        dropdownValue = data;
                      });
                    },
                    items: spinnerItems
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Column(
      children: <Widget>[
        _searchPhoto(),
        _sortPhoto(),
        Container(
          width: width,
          height: height * 0.55,
          color: Colors.grey,
          child: ListView(
            children: <Widget>[_itemPhoto()],
          ),
        ),
      ],
    );
  }
}
