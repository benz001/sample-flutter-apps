import 'package:flutter/material.dart';

class AlertDialogShow {
  Future<void> asyncConfirmDialog(BuildContext context, String title,
      String positive, String negative) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          // content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text(positive),
              onPressed: (){}
            ),
            FlatButton(
              child: Text(negative),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
}
